package com.john.toolbarbase.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.john.toolbarbase.R;

public class TheFragmentDialog extends BaseDialogFragment implements View.OnClickListener {
    private ImageView avatarImage;
    private TextView textView;
    private EditText editText1;
    private EditText editText2;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialogView=getActivity().getLayoutInflater().inflate(R.layout.dialog_layout, null, false);
        avatarImage=(ImageView)dialogView.findViewById(R.id.dialog_layout_avatarImage);
        textView=(TextView)dialogView.findViewById(R.id.dialog_layout_textView);
        editText1=(EditText)dialogView.findViewById(R.id.dialog_layout_editText1);
        editText2=(EditText)dialogView.findViewById(R.id.dialog_layout_editText2);

        AlertDialog alertDialog=new AlertDialog.Builder(getActivity())
                .setView(dialogView)
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", null)
                .setTitle("Fragment dialog")
                .show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(this);

        return alertDialog;
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(getActivity(), "You may have to use android.support.v7.app.AlertDialog", Toast.LENGTH_SHORT).show();
        dismiss();
    }
}
