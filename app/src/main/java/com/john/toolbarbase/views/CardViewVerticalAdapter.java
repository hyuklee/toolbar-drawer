package com.john.toolbarbase.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.john.toolbarbase.R;
import com.squareup.picasso.Picasso;

public class CardViewVerticalAdapter extends RecyclerView.Adapter<CardViewVerticalAdapter.ViewHolder> {
    private String[] captions;
    private int[] imageIds;
    private OnListener listener;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private CardView cardView;
        public ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView=cardView;
        }
    }//end ViewHolder;

    public CardViewVerticalAdapter(Context context, String[] captions, int[] imageIds) {
        this.context=context;
        this.captions = captions;
        this.imageIds = imageIds;
    }

    //when first constructed, it builds sets of viewHolders by repeatedly calling
    //onCreateViewHolder()
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the view
        CardView cardView=(CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_vertical, parent, false);
        return new ViewHolder(cardView);
    }

    //if the user scrolls the recyclerView onBindViewHolder() bind data to its contents
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView=holder.cardView;
        ImageView imageView=(ImageView)cardView.findViewById(R.id.card_view_image);
        Drawable drawable=cardView.getResources().getDrawable(imageIds[position]);
        imageView.setImageDrawable(drawable);
        imageView.setContentDescription(captions[position]);
        TextView textView=(TextView)cardView.findViewById(R.id.card_view_text);
        textView.setText(captions[position]);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return captions.length;
    }

    public void setListener(OnListener listener) {
        this.listener = listener;
    }

    public static interface OnListener {
        public void onClick(int position);
    }
}



















