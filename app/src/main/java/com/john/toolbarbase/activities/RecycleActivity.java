package com.john.toolbarbase.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.john.toolbarbase.R;
import com.john.toolbarbase.dialogs.FoodDialog;
import com.john.toolbarbase.infrastructure.Food;
import com.john.toolbarbase.views.CardViewVerticalAdapter;
import com.john.toolbarbase.views.MainNavDrawer;

public class RecycleActivity extends BaseActivity {
    @Override
    protected void onMyOnCreate() {
        setContentView(R.layout.recycle_activity);
        getSupportActionBar().setTitle(getResources().getString(R.string.recyler_activity));
        setNavDrawer(new MainNavDrawer(this));
        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycle_activity_recycler);

        //This could go on the BaseActivity but for this lesson I wanted to keep it here
        final String[] foods=new String[Food.foods.length];
        final int[] foodImgIds=new int[Food.foods.length];
        for(int i=0; i<foods.length; i++){
            foods[i]=Food.foods[i].getName();
            foodImgIds[i]=Food.foods[i].getImgResourceId();
        }

        CardViewVerticalAdapter cardViewVerticalAdapter =new CardViewVerticalAdapter(this, foods, foodImgIds);
        recyclerView.setAdapter(cardViewVerticalAdapter);
        LinearLayoutManager layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        cardViewVerticalAdapter.setListener(new CardViewVerticalAdapter.OnListener() {
            @Override
            public void onClick(int position) {
                FragmentTransaction fragmentTransaction=getSupportFragmentManager()
                        .beginTransaction().addToBackStack(null);
                FoodDialog dialog=new FoodDialog();
                Bundle bundle=new Bundle();
                bundle.putString(FoodDialog.FOOD_NAME, foods[position]);
                bundle.putInt(FoodDialog.FOOD_ID, foodImgIds[position]);
                dialog.setArguments(bundle);
                dialog.show(fragmentTransaction, null);

            }
        });
    }
}
