package com.john.toolbarbase.activities;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.john.toolbarbase.R;
import com.john.toolbarbase.infrastructure.Food;
import com.john.toolbarbase.views.ListViewAdapter;
import com.john.toolbarbase.views.MainNavDrawer;

public class ListActivity1 extends BaseActivity implements AdapterView.OnItemClickListener {
    public static float SELECTED_ITEM_TRANSLATION_X=100;
    private ListView listView;

    @Override
    protected void onMyOnCreate() {
        setContentView(R.layout.list_activity1);
        getSupportActionBar().setTitle(getResources().getString(R.string.list_activity1));
        setNavDrawer(new MainNavDrawer(this));

        listView=(ListView)findViewById(R.id.list_activity_listView);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        ListViewAdapter adapter=new ListViewAdapter(this, Food.foods, listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        float translationX;
        if(listView.isItemChecked(position)){
            translationX=SELECTED_ITEM_TRANSLATION_X;
        }else {
            translationX=0;
        }

        view.animate().translationX(translationX).setDuration(300).start();
    }
}











